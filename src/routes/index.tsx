import { Switch } from 'react-router-dom';
import Route from './Route';
import SignIn from '../screens/Login';
import SignUp from '../screens/Register';
import Schedule from '../screens/Schedule';
import Home from '../screens/Home';

const Routes: React.FC = () => (
  <Switch>
    <Route path="/" exact component={Home} />

    <Route path="/signin" component={SignIn} />
    <Route path="/signup" component={SignUp} />

    <Route path="/schedule" component={Schedule} isPrivate />
  </Switch>
);

export default Routes;
