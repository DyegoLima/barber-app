import Button from '@material-ui/core/Button';
import { Link } from 'react-router-dom';

const Home: React.FC = () => {
  return (
    <div className="App">
      Home Page
      <Link to="/login">
        <Button variant="contained" color="primary">
          Login
        </Button>
      </Link>
    </div>
  );
};

export default Home;
