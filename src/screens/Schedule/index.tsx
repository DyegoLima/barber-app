import React, {useState} from 'react';

import Button from '@material-ui/core/Button';


import DatePick from './components/DatePicker/DatePicker'
import Modal from './components/Modal'



function Schedule() {
 
  
  const [modalOpen, setModalOpen] = useState(false)

  const handleClick = () => {
    setModalOpen(true)
    
   
  }

  return (
    <div style={{ display: 'flex', justifyContent: 'center', alignItems: 'center', width: '100%', height: '97vh', backgroundImage: `url("https://img.elo7.com.br/product/main/1AF0424/papel-de-parede-adesivo-barbearia-salao-de-beleza.jpg")` }}>
      <div style={{ display: 'flex', flexDirection: 'column',    padding: '50px', borderColor: 'black', borderRadius: '8px', boxShadow: ' 6px 2px 19px 4px rgba(41, 50, 50, 0.46)', backgroundColor: '#FFF' }}>
       <DatePick         />
       <Button
          type="submit"
          fullWidth
          variant="contained"
          onClick={handleClick}
          color="primary"
        >
          Confirmar
        </Button>
        <Modal
            modalOpen={modalOpen}
            setModalOpen={setModalOpen}
        />
      </div>
    </div>
  );
}

export default Schedule;

