import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import DoneOutline from '@material-ui/icons/DoneOutline';
import Close from '@material-ui/icons/Close';

import { useHistory } from 'react-router-dom';

import { IconButton } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  paper: {
    top: '20%',
    left: '31%',
    position: 'absolute',
    width: 450,
    height: 350,
    backgroundColor: theme.palette.background.paper,
    borderRadius: '8px',
    padding: theme.spacing(1, 4, 3),
  },
  button: {
    margin: theme.spacing(1),
    textTransform: 'capitalize',
  },
  title: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  divIcons: {
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    height: '35vh',
  },
  icon: {
    fontSize: '2.5rem',
    color: '#2a8341',
  },
  iconBack: {
    fontSize: '2.5rem',
    color: '#eb1c1c',
    marginLeft: '10px',
  },
}));
const SimpleModal: React.FC = ({ modalOpen, setModalOpen }: any) => {
  const classes = useStyles();
  const history = useHistory();

  const handleClick = () => {
    history.push('/confirm');
  };
  const handleClose = () => {
    setModalOpen(false);
  };

  const body = (
    <div className={classes.paper}>
      <h2 id="simple-modal-title">Confirme seu agendamento!</h2>
      <p id="simple-modal-description" className={classes.title}>
        Duis mollis, est non commodo luctus, nisi erat porttitor ligula.
      </p>
      <SimpleModal />
      <div className={classes.divIcons}>
        <IconButton
          color="primary"
          aria-label="add an alarm"
          onClick={() => handleClick()}
          edge="end"
          className={classes.icon}
        >
          <DoneOutline />
        </IconButton>
        <IconButton
          color="primary"
          aria-label="add an alarm"
          onClick={() => setModalOpen(false)}
          edge="end"
          className={classes.iconBack}
        >
          <Close />
        </IconButton>
      </div>
    </div>
  );

  return (
    <div>
      <Modal
        open={modalOpen}
        onClose={handleClose}
        aria-labelledby="simple-modal-title"
        aria-describedby="simple-modal-description"
      >
        {body}
      </Modal>
    </div>
  );
};

export default SimpleModal;
