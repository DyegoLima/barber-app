import { useState } from 'react';
import DateFnsUtils from '@date-io/date-fns'; // choose your lib
import { MuiPickersUtilsProvider } from '@material-ui/pickers';

import format from 'date-fns/format';
import frLocale from 'date-fns/locale/pt-BR';
import { DateTimePicker } from './styles';

class LocalizedUtils extends DateFnsUtils {
  getDatePickerHeaderText(date: any) {
    // console.log(date)
    // console.log(format(date, "dd MM yyyy", { locale: this.locale }))
    return format(date, 'dd MM yyyy', { locale: this.locale });
  }
}

const Datepick = () => {
  const [selectedDate, setSelectedDate] = useState(new Date());

  console.log(`${selectedDate.getHours()}:${selectedDate.getMinutes()}`);
  console.log(
    `${selectedDate.getDay()}/${selectedDate.getMonth()}/${selectedDate.getFullYear()}`,
  );

  const handleDateChange = (e: any) => setSelectedDate(e);
  return (
    <MuiPickersUtilsProvider utils={LocalizedUtils} locale={frLocale}>
      <DateTimePicker
        value={selectedDate}
        onChange={(e: any) => handleDateChange(e)}
        allowKeyboardControl
        ampm
        animateYearScrolling={false}
        disablePast
        inputVariant="filled"
        variant="static"
        strictCompareDates
        minutesStep={15}
        disableToolbar={false}
        disableFuture={false}
        disabled
      />
    </MuiPickersUtilsProvider>
  );
};

export default Datepick;
