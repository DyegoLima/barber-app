import Home from './Home'
import Login from './Login'
import Register from './Register'
import Schedule from './Schedule'
import Confirm from './Confirm'
import Pagination from './Pagination'

export  const appRoutes = [
  { path: '/', component: Home },
  { path: '/login', component: Login, isPrivate: false },
  { path: '/register', component: Register, isPrivate: false },
  { path: '/schedule', component: Schedule, isPrivate: true },
  { path: '/confirm', component: Confirm, isPrivate: true },
  { path: '/pagination', component: Pagination, isPrivate: false }
]

