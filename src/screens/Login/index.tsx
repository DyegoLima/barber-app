import { useState } from 'react';
import { Link } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
// import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import MuiAlert from '@material-ui/lab/Alert';
import Snackbar from '@material-ui/core/Snackbar';

import api from '../../service/api';

const Alert = (props: any) => {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
};

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(24),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    boxShadow: ' 6px 2px 19px 4px rgba(41, 50, 50, 0.46)',
    width: '620px',
    height: '370px',
    padding: '32px',
    borderRadius: '8px',
    backgroundColor: '#FFF',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Login: React.FC = () => {
  const classes = useStyles();

  const [state, setState] = useState({});
  const [error, setError] = useState({ valueErro: false, errorMessage: '' });
  const [open, setOpen] = useState(false);

  const handleOnChange = (
    e: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>,
  ) => setState({ ...state, [e.target.name]: e.target.value });

  const handleSubmit = async (states: any) => {
    try {
      const result = await api.post('user/auth/costumer', states);
      api.defaults.headers.common.Authorization = result.data.token;
    } catch (err) {
      console.log(err);
    }
  };

  const handleClose = (event: any, reason: any) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };

  return (
    <div
      style={{
        width: '100%',
        height: '100vh',
        backgroundImage: `url("https://img.elo7.com.br/product/main/1AF0424/papel-de-parede-adesivo-barbearia-salao-de-beleza.jpg")`,
      }}
    >
      <Container
        component="main"
        style={{ display: 'flex', justifyContent: 'center' }}
      >
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h3">
            Um título
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              error={error.valueErro}
              label="Endereço de E-mail"
              name="username"
              onChange={e => handleOnChange(e)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              error={error.valueErro}
              fullWidth
              name="password"
              label="Password"
              type="password"
              onChange={e => handleOnChange(e)}
            />
            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => handleSubmit(state)}
            >
              Entrar
            </Button>
            <Grid container>
              <Grid item xs>
                {/* <Link href="#" variant="body2">
                  Forgot password?
                </Link> */}
              </Grid>
              <Grid item>
                <Link to="/register">Cadastrar</Link>
              </Grid>
            </Grid>
          </form>
          <Snackbar open={open} autoHideDuration={6000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="error">
              {error.errorMessage}
            </Alert>
          </Snackbar>
        </div>
      </Container>
    </div>
  );
};

export default Login;
