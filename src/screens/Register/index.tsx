import { useState } from 'react';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import { useHistory } from 'react-router-dom';
import api from '../../service/api';
import { phoneMask, dddMask } from '../../util/validation-mask';
import validateFields from '../../util/validate-fields';

const useStyles = makeStyles(theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

const Register: React.FC = () => {
  const classes = useStyles();
  const history = useHistory();
  const [state, setState] = useState({
    name: { value: '', validationError: false },
    lastName: { value: '', validationError: false },
    phoneDdd: { value: '', validationError: false },
    phone: { value: '', validationError: false },
    email: { value: '', validationError: false },
    password: { value: '', validationError: false },
    reciveEmail: { value: '', validationError: false },
  });

  const handleOnChange = e => {
    if (e.target.name === 'phone') e.target.value = phoneMask(e.target.value);
    if (e.target.name === 'phoneDdd') e.target.value = dddMask(e.target.value);
    setState({
      ...state,
      [e.target.name]: { value: e.target.value, validationError: false },
    });
  };

  const handleChecked = e => {
    setState({ ...state, [e.target.name]: { value: e.target.checked } });
  };

  const handleBlur = (e: any, validationError: any) => {
    setState({
      ...state,
      [e.target.name]: { value: e.target.value, validationError },
    });
  };

  const handleSubmit = async (e: any) => {
    try {
      const custumerDate = Object.keys(state).reduce((prev, curr) => {
        return { ...prev, ...{ [curr]: state[curr].value } };
      }, {});
      await api.post('/costumer', custumerDate);
      history.goBack('login');
    } catch (error) {
      console.log(error.response.data.message);
    }
  };

  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        height: '100vh',
        backgroundImage: `url("https://img.elo7.com.br/product/main/1AF0424/papel-de-parede-adesivo-barbearia-salao-de-beleza.jpg")`,
      }}
    >
      <Container
        component="main"
        maxWidth="xs"
        style={{
          backgroundColor: '#FFF',
          borderRadius: '8px',
          boxShadow: ' 6px 2px 19px 4px rgba(41, 50, 50, 0.46)',
        }}
      >
        <CssBaseline />
        <div className={classes.paper}>
          <Typography component="h1" variant="h3">
            Cadastre-se
          </Typography>
          <form className={classes.form} required>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  autoComplete="name"
                  name="name"
                  required
                  variant="outlined"
                  fullWidth
                  label="Nome"
                  error={state.name.validationError}
                  onBlur={e =>
                    handleBlur(e, validateFields.name.validate(e.target.value))}
                  helperText={state.name.validationError}
                  onChange={e => handleOnChange(e)}
                />
              </Grid>
              <Grid item xs={12} sm={6}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Sobrenome"
                  name="lastName"
                  autoComplete="lname"
                  error={!!state.lastName.validationError}
                  onBlur={e =>
                    handleBlur(
                      e,
                      validateFields.lastName.validate(e.target.value),
                    )}
                  helperText={state.lastName.validationError}
                  onChange={e => handleOnChange(e)}
                />
              </Grid>
              <Grid item xs={12} sm={3}>
                <TextField
                  autoComplete="phoneDdd"
                  name="phoneDdd"
                  variant="outlined"
                  required
                  fullWidth
                  id="phoneDdd"
                  label="DDD"
                  error={!!state.phoneDdd.validationError}
                  onBlur={e =>
                    handleBlur(
                      e,
                      validateFields.phoneDdd.validate(e.target.value),
                    )}
                  helperText={state.phoneDdd.validationError}
                  onChange={e => handleOnChange(e)}
                />
              </Grid>
              <Grid item xs={12} sm={9}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  id="phone"
                  label="Telefone"
                  name="phone"
                  autoComplete="phone"
                  error={!!state.phone.validationError}
                  onBlur={e =>
                    handleBlur(e, validateFields.phone.validate(e.target.value))
                  }
                  helperText={state.phone.validationError}
                  onChange={e => handleOnChange(e)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  label="Endereço de e-mail"
                  name="email"
                  onBlur={e =>
                    handleBlur(e, validateFields.email.validate(e.target.value))}
                  error={!!state.email.validationError}
                  helperText={state.email.validationError}
                  onChange={e => handleOnChange(e)}
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  onBlur={e =>
                    handleBlur(
                      e,
                      validateFields.password.validate(e.target.value),
                    )}
                  error={!!state.password.validationError}
                  helperText={state.password.validationError}
                  onChange={e => handleOnChange(e)}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={(
                    <Checkbox
                      color="primary"
                      name="reciveEmail"
                      onChange={e => handleChecked(e)}
                    />
                  )}
                  label="Quero receber promoçõese atualizações de preço por e-mail"
                />
              </Grid>
            </Grid>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={e => handleSubmit(e)}
            >
              Cadastrar
            </Button>
          </form>
        </div>
      </Container>
    </div>
  );
};

export default Register;
