export const cpfMask = (value: string) => value === undefined
  ? ''
  : value
    .replace(/\D/g, '')
    .replace(/(\d{3})(\d)/, '$1.$2')
    .replace(/(\d{3})(\d)/, '$1.$2')
    .replace(/(\d{3})(\d{1,2})/, '$1-$2')
    .replace(/(-\d{2})\d+?$/, '$1')

export const cnpjMask = (value: string) => value === undefined
  ? ''
  : value
    .replace(/\D/g, '')
    .replace(/^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/, '$1.$2.$3/$4-$5')

export const postalCodeMask = (value: string) => value === undefined
  ? ''
  : value
    .replace(/\D/g, '')
    .replace(/(\d{5})(\d)/, '$1-$2')
    .replace(/(-\d{3})\d+?$/, '$1')

export const phoneMask = (value: string) => {
  if (value === undefined) {
    return ''
  }
  if (value.length <= 9) {
    return value.replace(/\D/g, '')
      .replace(/(\d{4})(\d)/, '$1-$2')
      .replace(/(-\d{4})\d+?$/, '$1')
  }

  return value.replace(/\D/g, '')
    .replace(/(\d{5})(\d)/, '$1-$2')
    .replace(/(-\d{4})\d+?$/, '$1')
}

export const numberMask = (value: string) => value === undefined
  ? ''
  : value
    .replace(/\D/g, '')

export const dddMask = (value: string) => value === undefined
  ? ''
  : value
    .replace(/\D/g, '')
    .replace(/^(\d{1})(\d{2})/, '0$2')

export const dateMask = (value: string) => value === undefined
  ? ''
  : value
    .replace(/\D/g, '')
    .replace(/(\d{2})(\d)/, '$1/$2')
    .replace(/(\d{2})(\d)/, '$1/$2')
    .replace(/(\d{4})/, '$1')
