import {
  isValidEmail,
  isValidPhone
} from '@brazilian-utils/brazilian-utils'

export default {
  name: {
    validate: (v: any) => {
      if (!v) return 'Campo obrigatório'
      return false
    }
  },
  password: {
    validate: (v: any) => {
      if (!v) return 'Campo obrigatório'
      return false
    }
  },

  lastName: {
    validate: (v: any) => {
      if (!v) return 'Campo obrigatório'
      return false
    }
  },
  phoneDdd: {
    validate: (v: any) => {
      if (!v) return 'Campo obrigatório'
      if (v.length < 3) return 'Campo DDD deve conter 3 caracteres'
      return false
    }
  },

  phone: {
    validate: (v: any) => {
      if (!v) return 'Campo obrigatório'
      if (v.length < 3) return 'Campo phone  deve conter 8 ou 9 caracteres'
      if (!isValidPhone(`11${v}`)) return 'Campo Telefone é inválido'
      return false
    }
  },
  
  email: {
    validate: (v: any) => {
      if (!v) return 'Campo obrigatório'
      if (!isValidEmail(v)) return 'Campo E-mail é inválido'
      return false
    }
  }
}
